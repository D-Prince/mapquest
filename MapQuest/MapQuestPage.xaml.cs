﻿using Xamarin.Forms;

namespace MapQuest
{
    public partial class MapQuestPage : ContentPage
    {
        public MapQuestPage()
        {
            InitializeComponent();
        }

        void GoToMapClicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new MapWithPins());
        }
    }
}

﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MapQuest
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapWithPins : ContentPage
    {
        public MapWithPins()
        {
            InitializeComponent();

            var initialMapLocation = MapSpan.FromCenterAndRadius
                                           (new Position(33.1307785, -117.1601826)
                                            , Distance.FromMiles(1));
       
            mapName.MoveToRegion(initialMapLocation);

            PlaceAMarker();

            PopulatePicker();
        
        }

        private void PlaceAMarker()
        {
            var position1 = new Position(33.478132, -117.101967); // Latitude, Longitude
            var pin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Hana Sushi",
                Address = "custom detail info"
            };
            mapName.Pins.Add(pin1);

            var position2 = new Position(33.555807, -117.216891);
            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "Public House",
                Address = "custom detail info"
            };
            mapName.Pins.Add(pin2);

            var position3 = new Position(33.660062, -117.298193);
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "Vincenzo's Olive Tree",
                Address = "custom detail info"
            };
            mapName.Pins.Add(pin3);

            var position4 = new Position(33.550755, -117.194920);
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "The Spicy Noodle",
                Address = "custom detail info"
            };
            mapName.Pins.Add(pin4);

            var position5 = new Position(32.980314, -117.259781);
            var pin5 = new Pin
            {
                Type = PinType.Place,
                Position = position5,
                Label = "Pamplemousse Grill",
                Address = "custom detail info"
            };
            mapName.Pins.Add(pin5);
        }
           
            private void PopulatePicker()
            {
                Dictionary<string, Color> nameToColor = new Dictionary<string, Color>()
            {
                { "Pin1" , Color.Red },
                { "Pin2", Color.Gray },
                { "Pin3", Color.Lime },
                { "Pin4", Color.Navy },
                { "Pin5", Color.Purple },

            };


                foreach (var item in nameToColor)
                {
                    pickerName.Items.Add(item.Key);
                }

                pickerName.SelectedIndex = 0;


            
       }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if(pickerName.SelectedIndex == 0)
            {
                var hanaSushiPin = MapSpan.FromCenterAndRadius
                                                (new Position(33.478132, -117.101967)
                                            , Distance.FromMiles(1));
                mapName.MoveToRegion(hanaSushiPin);
            }

            else if(pickerName.SelectedIndex == 1)
            {
                var publicHousePin = MapSpan.FromCenterAndRadius
                                            (new Position(33.555807, -117.216891)
                                            , Distance.FromMiles(1));
                mapName.MoveToRegion(publicHousePin);
            }
           
            else if (pickerName.SelectedIndex == 2)
            {
                var vincenzosPin = MapSpan.FromCenterAndRadius
                                            (new Position(33.660062, -117.298193)
                                            , Distance.FromMiles(1));
                mapName.MoveToRegion(vincenzosPin);
            }

            else if (pickerName.SelectedIndex == 3)
            {
                var spicyNoodlePin = MapSpan.FromCenterAndRadius
                                            (new Position(33.550755, -117.194920)
                                            , Distance.FromMiles(1));
                mapName.MoveToRegion(spicyNoodlePin);

            }
           
            else if (pickerName.SelectedIndex == 4)
            {
                var pamplemoussePin = MapSpan.FromCenterAndRadius
                                             (new Position(32.980314, -117.259781)
                                            , Distance.FromMiles(1));
                mapName.MoveToRegion(pamplemoussePin);
            }
        }

        void streetViewClicked(object sender, System.EventArgs e)
        {
            mapName.MapType = MapType.Street;
        }

        void satelliteClicked(object sender, System.EventArgs e)
        {
            mapName.MapType = MapType.Satellite;
        }

        void hybridClicked(object sender, System.EventArgs e)
        {
            mapName.MapType = MapType.Hybrid;
        }
    }
}
